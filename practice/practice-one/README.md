## Overview

- This document provides the requirement for HTML CSS practice one.
- Design: [Figma](<https://www.figma.com/file/BqOR5d14AwutOqJUCmbwpt/School-Genuine---responsive-website%C2%A0template-download-html-with%C2%A0css-for-school-(Community)?node-id=2415%3A26437&t=38UeeLROr8J9tDbJ-0>)

## Technical

- HTML/CSS

## Timeline

- 7 days (+2 days)

## Team size

- 1 dev

## Editor

- Visual Studio Code

## REQUIREMENTS

- Use the right HTML tags
- Apply Flexbox
- Apply HTML inspector
- Apply CSS Guideline
- Apply Technical parcel

## Targets

- Understand HTML/CSS properties and use them correctly
- Slice from Figma to HTML/CSS
- Understand the concepts of Flex

### How to run practice one:

- $ git clone --branch feature/practice-one https://gitlab.com/repsweet080303/html-css-training.git

- cd html-css-training/practice/practice-one

> npm install parcel (if you not install parcel)

> npm start

follow at:

http://localhost:1234/
