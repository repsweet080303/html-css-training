## Overview

- This document provides the requirement for HTML CSS practice two.
- Design: [Figma](<https://www.figma.com/file/BqOR5d14AwutOqJUCmbwpt/School-Genuine---responsive-website%C2%A0template-download-html-with%C2%A0css-for-school-(Community)?node-id=2415%3A26437&t=38UeeLROr8J9tDbJ-0>)

## Technical

- HTML/CSS
- Bootstrap
- SCSS

## Timeline

- 11 days

## Team size

- 1 dev

## Editor

- Visual Studio Code

## REQUIREMENTS

- Apply responsive to practice one
- Apply SCSS
- Apply Bootstrap

## Targets

- Use media queries
- Use framework bootstrap build website
- Apply SCSS
- Understand design screen tablet, mobile and pc

### How to run practice two:

- $ git clone --branch feature/practice-two https://gitlab.com/repsweet080303/html-css-training.git

- cd html-css-training/practice/practice-two

> npm install

> npm start

follow at:

http://localhost:1234/
